//--- CODE --------------------------
var foo = 'bar';

//--- SPECS -------------------------
describe("foo", function() {
  it("has a value of bar", function() {
    expect(foo).toBe('bar');
  });
});

///////////////////////////


var ourDataArray = {
    ControlPanel: {
    CurrentInOrder: 1
    },
    Questions: [{
        Text: "Do we like to test?"
    }, {
        Text: "Do we have to test?"
    }, {
        Text: "Why do we test?"
    }, {
        Text: "When do we test?"
    }]
};

describe("Our data array", function() {
  it("has four items", function() {
    expect(ourDataArray.Questions.length).toBe(4);
  });
});

describe("Our data array", function() {
  it("has two properties", function() {
    expect(Object.keys(ourDataArray).length).toBe(2);
  });
});

//////////////////

//http://opensas.wordpress.com/2013/06/23/sharing-you-javascript-ninja-secrets-run-your-jasmine-tests-on-jsfiddle/

describe("running jasmine in jsfiddle", function(){
    it("should run tests", function(){
        expect(true).toEqual(true);
    });
});

// load jasmine htmlReporter
(function() {
  var env = jasmine.getEnv();
  env.addReporter(new jasmine.HtmlReporter());
  env.execute();
}());